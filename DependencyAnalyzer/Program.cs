﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DependencyAnalyzer
{
	static class Program
	{
		private static Version _version;

		static Program()
		{
			Program._version = Assembly.GetExecutingAssembly().GetName().Version;
		}
		/// <summary>
		/// Der Haupteinstiegspunkt für die Anwendung.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if(args != null && args.Length == 1)
			{
				string settingsFilePath = args[0];

				AnalyzeSettings settings = GetSettings(settingsFilePath);

				if(settings != null)
				{
					Analyze(settings.AssemblyFilePath, settings.DependenciesFilePath, settings.UnneededAssembliesFilePath, settings.AssemblyCacheFolderPath);
				}
			}
		}

		public static Version GetVersion()
		{
			return _version;
		}

		private static AnalyzeSettings GetSettings(string settingsFilePath)
		{
			AnalyzeSettings settings = null;

			using(FileStream fs = new FileStream(settingsFilePath, FileMode.Open, FileAccess.Read, FileShare.None))
			{
				using(StreamReader sr = new StreamReader(fs))
				{
					string assemblyFilePath = sr.ReadLine();
					string dependenciesFilePath = sr.ReadLine();
					string unneededAssembliesFilePath = sr.ReadLine();
					string assemblyCacheFolderPath = sr.ReadLine();

					settings = new AnalyzeSettings(assemblyFilePath, dependenciesFilePath, unneededAssembliesFilePath, assemblyCacheFolderPath);
				}
			}

			return settings;
		}

		private static void Analyze(string filePath, string resultFileDependencies, string resultFileUnneededAssemblies, string assemblyCacheFolder)
		{
			dcutils.DependencyCrawler dependencyCrawler = new dcutils.DependencyCrawler(filePath, assemblyCacheFolder);
			List<dcutils.AssemblyData> dependencies = dependencyCrawler.GetAllDependencies();

			using(FileStream fs = new FileStream(resultFileDependencies, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				using(StreamWriter sw = new StreamWriter(fs))
				{
					foreach(dcutils.AssemblyData ad in dependencies)
					{
						sw.WriteLine(ad.ToString());
					}
				}
			}

			List<dcutils.AssemblyData> unneededAssemblies = dependencyCrawler.GetAllUnneededAssemblies(dependencies.ToArray());

			using(FileStream fs = new FileStream(resultFileUnneededAssemblies, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				using(StreamWriter sw = new StreamWriter(fs))
				{
					foreach(dcutils.AssemblyData ua in unneededAssemblies)
					{
						sw.WriteLine(ua.ToString());
					}
				}
			}
		}
	}
}