﻿namespace Deependency
{
    partial class WndColumnSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.chkFileInLocalFolderOrGAC = new System.Windows.Forms.CheckBox();
			this.chkProcessorArchitecture = new System.Windows.Forms.CheckBox();
			this.chkFileInProgramFolder = new System.Windows.Forms.CheckBox();
			this.chkCulture = new System.Windows.Forms.CheckBox();
			this.chkPublicKeyToken = new System.Windows.Forms.CheckBox();
			this.chkVersion = new System.Windows.Forms.CheckBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// chkFileInLocalFolderOrGAC
			// 
			this.chkFileInLocalFolderOrGAC.AutoSize = true;
			this.chkFileInLocalFolderOrGAC.Location = new System.Drawing.Point(6, 127);
			this.chkFileInLocalFolderOrGAC.Name = "chkFileInLocalFolderOrGAC";
			this.chkFileInLocalFolderOrGAC.Size = new System.Drawing.Size(169, 17);
			this.chkFileInLocalFolderOrGAC.TabIndex = 17;
			this.chkFileInLocalFolderOrGAC.Text = "Location in local folder or GAC";
			this.chkFileInLocalFolderOrGAC.UseVisualStyleBackColor = true;
			// 
			// chkProcessorArchitecture
			// 
			this.chkProcessorArchitecture.AutoSize = true;
			this.chkProcessorArchitecture.Location = new System.Drawing.Point(6, 35);
			this.chkProcessorArchitecture.Name = "chkProcessorArchitecture";
			this.chkProcessorArchitecture.Size = new System.Drawing.Size(65, 17);
			this.chkProcessorArchitecture.TabIndex = 15;
			this.chkProcessorArchitecture.Text = "x86/x64";
			this.chkProcessorArchitecture.UseVisualStyleBackColor = true;
			// 
			// chkFileInProgramFolder
			// 
			this.chkFileInProgramFolder.AutoSize = true;
			this.chkFileInProgramFolder.Location = new System.Drawing.Point(6, 104);
			this.chkFileInProgramFolder.Name = "chkFileInProgramFolder";
			this.chkFileInProgramFolder.Size = new System.Drawing.Size(148, 17);
			this.chkFileInProgramFolder.TabIndex = 16;
			this.chkFileInProgramFolder.Text = "Location in program folder";
			this.chkFileInProgramFolder.UseVisualStyleBackColor = true;
			// 
			// chkCulture
			// 
			this.chkCulture.AutoSize = true;
			this.chkCulture.Location = new System.Drawing.Point(6, 58);
			this.chkCulture.Name = "chkCulture";
			this.chkCulture.Size = new System.Drawing.Size(59, 17);
			this.chkCulture.TabIndex = 12;
			this.chkCulture.Text = "Culture";
			this.chkCulture.UseVisualStyleBackColor = true;
			// 
			// chkPublicKeyToken
			// 
			this.chkPublicKeyToken.AutoSize = true;
			this.chkPublicKeyToken.Location = new System.Drawing.Point(6, 81);
			this.chkPublicKeyToken.Name = "chkPublicKeyToken";
			this.chkPublicKeyToken.Size = new System.Drawing.Size(104, 17);
			this.chkPublicKeyToken.TabIndex = 13;
			this.chkPublicKeyToken.Text = "PublicKeyToken";
			this.chkPublicKeyToken.UseVisualStyleBackColor = true;
			// 
			// chkVersion
			// 
			this.chkVersion.AutoSize = true;
			this.chkVersion.Location = new System.Drawing.Point(6, 12);
			this.chkVersion.Name = "chkVersion";
			this.chkVersion.Size = new System.Drawing.Size(61, 17);
			this.chkVersion.TabIndex = 14;
			this.chkVersion.Text = "Version";
			this.chkVersion.UseVisualStyleBackColor = true;
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(164, 153);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 18;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// WndColumnSelection
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(251, 188);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.chkFileInLocalFolderOrGAC);
			this.Controls.Add(this.chkProcessorArchitecture);
			this.Controls.Add(this.chkFileInProgramFolder);
			this.Controls.Add(this.chkCulture);
			this.Controls.Add(this.chkPublicKeyToken);
			this.Controls.Add(this.chkVersion);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(267, 226);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(267, 226);
			this.Name = "WndColumnSelection";
			this.Text = "Column Selection";
			this.Load += new System.EventHandler(this.WndColumnSelection_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkFileInLocalFolderOrGAC;
        private System.Windows.Forms.CheckBox chkProcessorArchitecture;
        private System.Windows.Forms.CheckBox chkFileInProgramFolder;
        private System.Windows.Forms.CheckBox chkCulture;
        private System.Windows.Forms.CheckBox chkPublicKeyToken;
        private System.Windows.Forms.CheckBox chkVersion;
        private System.Windows.Forms.Button btnSave;
    }
}