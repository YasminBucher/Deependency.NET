﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deependency
{
	public partial class WndMain : Form
	{
		private const string FOLDERNAME_APPDATA = "DependencyCrawler";
		private const string FOLDERNAME_ASSEMBLYCACHE = "AssemblyCache";
		private const string FILENAME_DEPENDENCIES = "dependencies.csv";
		private const string FILENAME_UNNEEDEDASSEMBLIES = "unneededAssemblies.csv";
		private const string FILENAME_SETTINGS = "settings.txt";

		private static readonly string _appDataFolder = null;
		private static readonly string _assemblyCacheFolder = null;
		private static readonly string _dependenciesFilePath = null;
		private static readonly string _unneededAssembliesFilePath = null;
		private static readonly string _settingsFilePath = null;

		static WndMain()
		{
			_appDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), FOLDERNAME_APPDATA);

			if(!Directory.Exists(_appDataFolder))
			{
				Directory.CreateDirectory(_appDataFolder);
			}

			_assemblyCacheFolder = Path.Combine(_appDataFolder, FOLDERNAME_ASSEMBLYCACHE);

			if(!Directory.Exists(_assemblyCacheFolder))
			{
				Directory.CreateDirectory(_assemblyCacheFolder);
			}

			_dependenciesFilePath = Path.Combine(_appDataFolder, FILENAME_DEPENDENCIES);
			_unneededAssembliesFilePath = Path.Combine(_appDataFolder, FILENAME_UNNEEDEDASSEMBLIES);
			_settingsFilePath = Path.Combine(_appDataFolder, FILENAME_SETTINGS);
		}

		public WndMain()
		{
			InitializeComponent();
			this.DoubleBuffered = true;
		}

		private void btnSelectAssembly_Click(object sender, EventArgs e)
		{
			this.SelectAssembly();
		}

		private void btnAnalyze_Click(object sender, EventArgs e)
		{
			this.Analyze();
		}

		private void SelectAssembly()
		{
			using(OpenFileDialog ofd = new OpenFileDialog())
			{
				ofd.Title = "Select Assembly";
				ofd.CheckFileExists = true;
				DialogResult dr = ofd.ShowDialog();

				string filePath = null;

				if(dr == DialogResult.OK)
				{
					filePath = ofd.FileName;
				}

				if(filePath != null)
				{
					if(!File.Exists(filePath))
					{
						MBox.Warning("Please select an existing file!");
					}
					else
					{
						this.txtSelectAssembly.Text = filePath;
					}
				}
			}
		}

		private void Analyze()
		{
			this.dgvDependencies.Rows.Clear();
			this.dgvUnneededAssemblies.Rows.Clear();
			this.ClearAssemblyCache();
			this.DeleteResultFiles();

			string filePath = txtSelectAssembly.Text.Trim();

			if(!File.Exists(filePath))
			{
				MBox.Warning("Please select an existing file!");
				return;
			}

			this.CopyAppAssembliesToCache(filePath);
			this.CreateSettingsFile(filePath);

			using(Process process = new Process())
			{
				string startupParams = string.Format("{0}", QuoteString(_settingsFilePath));
				process.StartInfo = new ProcessStartInfo("DependencyAnalyzer.exe", startupParams);
				process.Start();
				process.WaitForExit();
			}

			this.AnalyzeDependencies();
			this.AnalyzeUnneededAssemblies();
			this.ClearAssemblyCache();
		}

		private void CreateSettingsFile(string assemblyFilePath)
		{
			using(FileStream fs = new FileStream(_settingsFilePath, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				using(StreamWriter sw = new StreamWriter(fs))
				{
					sw.WriteLine(assemblyFilePath);
					sw.WriteLine(_dependenciesFilePath);
					sw.WriteLine(_unneededAssembliesFilePath);
					sw.WriteLine(_assemblyCacheFolder);
				}
			}
		}

		private static string QuoteString(string s)
		{
			return String.Format("\"{0}\"", s);
		}

		private void AnalyzeDependencies()
		{
			List<dcutils.AssemblyData> dependencies = this.ParseAssemblyDataFile(_dependenciesFilePath);

			if(dependencies == null)
			{
				MBox.Warning("Unable to analyze assembly dependencies!");
				return;
			}

			foreach(dcutils.AssemblyData dependency in dependencies)
			{
				DataGridViewRow row = new DataGridViewRow();
				row.CreateCells(dgvDependencies,
					dependency.Name,
					dependency.Version,
					dependency.ProcessorArchitecture,
					dependency.CultureName,
					dependency.PublicTokenKey,
					dependency.FoundInProgramFolder,
					dependency.FoundInLocalFolderOrGAC,
					dependency.FileNameInProgramFolder,
					dependency.FileNameInLocalFolderOrGAC);
				row.Tag = dependency;

				this.dgvDependencies.Rows.Add(row);
			}
		}

		private void AnalyzeUnneededAssemblies()
		{
			List<dcutils.AssemblyData> unneededAssemblies = this.ParseAssemblyDataFile(_unneededAssembliesFilePath);

			if(unneededAssemblies == null)
			{
				MBox.Warning("Unable to analyze unneeded assemblies!");
				return;
			}

			foreach(dcutils.AssemblyData unneededAssembly in unneededAssemblies)
			{
				DataGridViewRow row = new DataGridViewRow();
				row.CreateCells(dgvUnneededAssemblies,
					unneededAssembly.Name,
					unneededAssembly.Version,
					unneededAssembly.ProcessorArchitecture,
					unneededAssembly.CultureName,
					unneededAssembly.PublicTokenKey,
					unneededAssembly.FoundInProgramFolder,
					unneededAssembly.FileNameInProgramFolder);
				row.Tag = unneededAssembly;

				this.dgvUnneededAssemblies.Rows.Add(row);
			}
		}

		private List<dcutils.AssemblyData> ParseAssemblyDataFile(string file)
		{
			if(!File.Exists(file))
			{
				return null;
			}

			List<dcutils.AssemblyData> dependencies = new List<dcutils.AssemblyData>();

			using(FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
			{
				using(StreamReader sr = new StreamReader(fs))
				{
					string line = null;

					while((line = sr.ReadLine()) != null)
					{
						dcutils.AssemblyData ad = dcutils.AssemblyData.Parse(line);
						dependencies.Add(ad);
					}
				}
			}

			return dependencies;
		}

		public void ClearAssemblyCache()
		{
			(new DirectoryInfo(_assemblyCacheFolder)).GetFiles().ToList().ForEach(fi => fi.Delete());
		}

		private void CopyAppAssembliesToCache(string assemblyPath)
		{
			this.GetAllAppAssemblies(assemblyPath).ForEach(fi => fi.CopyTo(Path.Combine(_assemblyCacheFolder, fi.Name)));
		}

		private List<FileInfo> GetAllAppAssemblies(string assemblyPath)
		{
			List<FileInfo> appAssemblies = new List<FileInfo>();

			string _assemblyFolderPath = Path.GetDirectoryName(assemblyPath);
			appAssemblies.AddRange((new DirectoryInfo(_assemblyFolderPath)).EnumerateFiles().ToList().Where(fi => dcutils.DependencyCrawler._assemblyFileExtensions.Contains(fi.Extension)));

			return appAssemblies;
		}

		private void DeleteResultFiles()
		{
			if(File.Exists(_dependenciesFilePath))
			{
				File.Delete(_dependenciesFilePath);
			}

			if(File.Exists(_unneededAssembliesFilePath))
			{
				File.Delete(_unneededAssembliesFilePath);
			}
		}

		private void tsmiColumnSelection_Click(object sender, EventArgs e)
		{
			using(WndColumnSelection colSel = new WndColumnSelection())
			{
				if(colSel.ShowDialog() == DialogResult.OK)
				{
					this.SetOptions();
				}
			}
		}

		private void SetOptions()
		{
			this.dgvcDependencyAssemblyVersion.Visible = Properties.Settings.Default.ShowVersionColumn;
			this.dgvcUnneededAssemblyVersion.Visible = Properties.Settings.Default.ShowVersionColumn;

			this.dgvcDependencyAssemblyCulture.Visible = Properties.Settings.Default.ShowCultureColumn;
			this.dgvcUnneededAssemblyCulture.Visible = Properties.Settings.Default.ShowCultureColumn;

			this.dgvcDependencyAssemblyProcessorArchitecture.Visible = Properties.Settings.Default.ShowProcessorArchitectureColumn;
			this.dgvcUnneededAssemblyProcessorArchitecture.Visible = Properties.Settings.Default.ShowProcessorArchitectureColumn;

			this.dgvcDependencyAssemblyPublicKeyToken.Visible = Properties.Settings.Default.ShowPublicKeyTokenColumn;
			this.dgvcUnneededAssemblyPublicKeyToken.Visible = Properties.Settings.Default.ShowPublicKeyTokenColumn;

			this.dgvcDependencyFileInProgramFolder.Visible = Properties.Settings.Default.ShowFileInProgramFolderColumn;
			this.dgvcUnneededFileInProgramFolder.Visible = Properties.Settings.Default.ShowFileInProgramFolderColumn;

			this.dgvcDependencyFileInLocalFolderOrGAC.Visible = Properties.Settings.Default.ShowFileInLocalFolderOrGACColumn;
		}

		private void tsmiClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void WndMain_Load(object sender, EventArgs e)
		{
			this.SetOptions();
		}
	}
}