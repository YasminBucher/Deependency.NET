﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deependency
{
	class MBox
	{
		public static void Info(string text)
		{
			Info("Information", text);
		}

		public static void Info(string title, string text)
		{
			MessageBox.Show(text, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public static void Warning(string text)
		{
			Warning("Warning", text);
		}

		public static void Warning(string title, string text)
		{
			MessageBox.Show(text, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		public static void Error(string text)
		{
			Error("Error", text);
		}

		public static void Error(string title, string text)
		{
			MessageBox.Show(text, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}
}