﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dcutils
{
	public class Utils
	{
		public static string ByteArrayToHexString(byte[] byteArray)
		{
			return BitConverter.ToString(byteArray).Replace('-', ' ');
		}

		public static byte[] HexStringToByteArray(string s)
		{
			return s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(part => byte.Parse(part, System.Globalization.NumberStyles.AllowHexSpecifier)).ToArray();
		}
	}
}